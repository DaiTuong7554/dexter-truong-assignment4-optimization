﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    //Variables for the prefab
    public GameObject Prefab;

    //The fire rate at which the player can fire prefabs
    public float FireSpeed = 5;

    //Boolean for checking if the player can shoot a ball
    public bool CanShoot;

    // Update is called once per frame
    void Update()
    {
        //If the left mouse button is pressed, fire a prefab forward
        if ( Input.GetButton( "Fire1" )&&(!CanShoot) )
        {
            StartCoroutine(FireShot());
        }
    }


    IEnumerator FireShot()
    {

        CanShoot = true;
        ShootBall();
        yield return new WaitForSeconds(0.4f);
        CanShoot = false;

    }


    public void ShootBall()
    {
        //Gets point where the prefab can fire that's a vector
        //The point is acquired from where the mouse is clicked on the camera in relation the screen
        Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition + Vector3.forward);

        //Sets firing direction of the prefab to where the player clicks
        Vector3 FireDirection = clickPoint - this.transform.position;

        //Normalizes the firing direction of the prefab
        FireDirection.Normalize();

        //Instantiates the prefab
        GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);

        //Acquires the rigidbody component and fires the prefab through physics by the firerate 
        prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
    }

}
