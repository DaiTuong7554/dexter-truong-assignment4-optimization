﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    //Ths script changes the colours of the environment

    // Use this for initialization
    void Start()
    {
        //Gets the material from the props in the environment
        //Randomizes the colour of the props
        GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
    }

}
