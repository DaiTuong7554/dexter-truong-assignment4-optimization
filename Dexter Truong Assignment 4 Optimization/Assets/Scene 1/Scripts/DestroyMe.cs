﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour
{
    public float aliveTime;

    // Use this for initialization
    void Update()
    {
        Destroy(gameObject, aliveTime);
    }
}
