﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    //The collectible prefab
    public GameObject Collectible;
    
    //Prefab for the current collectible
    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        //If the current amount of collectibles is not existant, instantiate a collectible
        if ( m_currentCollectible == null )
        {
            //Instatiates a collectible at a random position and acquires the transform of the child of the GameObject this script is attached to
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
        }
    }
}
