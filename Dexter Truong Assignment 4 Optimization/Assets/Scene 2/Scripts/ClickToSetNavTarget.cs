﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    //This script moves the player character via NavMesh

    //The speed of the player
    public float speed;

	// Update is called once per frame
	void Update ()
    {
        //Activates keyboard movement function
        MoveByKeyboard();
	}

    //Moves the player by keyboard movement (WASD)
    public void MoveByKeyboard()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey("w"))
        {
            pos.z += speed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            pos.z -= speed * Time.deltaTime;
        }
        if (Input.GetKey("d"))
        {
            pos.x += speed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            pos.x -= speed * Time.deltaTime;
        }


        transform.position = pos;
    }


    public void MoveByNavmesh()
    {
        //Acquires camera component
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();

        //Calls raycast
        RaycastHit hit = new RaycastHit();

        //Uses raycast that points to where the player clicks on the screen
        //Detects the raycast from mouse click for player movement only if the ground layer is clicked
        if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
        {
            //Gets component for NavMesh
            //Using NavMesh, moves the player character to wherever the mouse is clicked
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
    }
}
