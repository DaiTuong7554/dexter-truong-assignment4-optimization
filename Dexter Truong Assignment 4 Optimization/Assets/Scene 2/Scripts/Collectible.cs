﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    //Radius variable for detecting pickup of the collectible
    public float Radius;


    //New collision with OnCollisionEnter
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            print("yes");
            Destroy(this.gameObject);
        }
    }


    //Original code for collision with physics
    public void CollideWithPhysics()
    {
        //Collider class that creates a sphere around the object
        Collider[] collidingColliders = Physics.OverlapSphere(this.transform.position, Radius);

        //If the player touches the collectible, destroy the collectible
        for (int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex)
        {
            if (collidingColliders[colliderIndex].tag == "Player")
            {
                Destroy(this.gameObject);
            }
        }
    }
}
